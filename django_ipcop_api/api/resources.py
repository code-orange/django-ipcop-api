import ipaddress
from django.contrib.auth.models import User
from django.db.models import signals
from itertools import chain
from tastypie import fields
from tastypie.models import create_api_key
from tastypie.resources import Resource, ModelResource
from tastypie.serializers import Serializer
from urllib import parse

from django_cdstack_deploy.django_cdstack_deploy.authentification import (
    HostApiKeyAuthentication,
)
from django_ipcop_models.django_ipcop_models.models import *

signals.post_save.connect(create_api_key, sender=User)


class UrlencodeSerializer(Serializer):
    formats = ["json", "jsonp", "xml", "yaml", "html", "plist", "urlencode"]
    content_types = {
        "json": "application/json",
        "jsonp": "text/javascript",
        "xml": "application/xml",
        "yaml": "text/yaml",
        "html": "text/html",
        "plist": "application/x-plist",
        "urlencode": "application/x-www-form-urlencoded",
    }

    def from_urlencode(self, data, options=None):
        """handles basic formencoded url posts"""
        qs = dict(
            (k, v if len(v) > 1 else v[0]) for k, v in parse.parse_qs(data).items()
        )
        return qs

    def to_urlencode(self, content):
        pass


class CheckIpResource(ModelResource):
    class Meta:
        queryset = IpcopKnownIp4.objects.all()
        allowed_methods = ["get"]


class ReportResource(Resource):
    comment = fields.CharField(attribute="comment")
    ip = fields.CharField(attribute="ip")
    category = fields.ListField(attribute="category")

    class Meta:
        queryset = IpcopKnownIp4.objects.all()
        allowed_methods = ["post"]
        authentication = HostApiKeyAuthentication()
        serializer = UrlencodeSerializer()

    def obj_create(self, bundle, **kwargs):
        ip_address = ipaddress.ip_address(bundle.data["ip"])
        bundle.obj = None

        if isinstance(ip_address, ipaddress.IPv4Address):
            try:
                bundle.related_obj = IpcopKnownIp4.objects.get(ipaddr=str(ip_address))
            except IpcopKnownIp4.DoesNotExist:
                bundle.related_obj = IpcopKnownIp4(ipaddr=str(ip_address))

            bundle.obj = IpcopAbuseIp4()
        elif isinstance(ip_address, ipaddress.IPv6Address):
            try:
                bundle.related_obj = IpcopKnownIp6.objects.get(ipaddr=str(ip_address))
            except IpcopKnownIp6.DoesNotExist:
                bundle.related_obj = IpcopKnownIp6(ipaddr=str(ip_address))

            bundle.obj = IpcopAbuseIp6()
        else:
            # TODO: Handles errors here
            return bundle

        bundle.related_obj.save()

        # get category
        cat_split = bundle.data["category"].split(",")

        categories = list()

        if len(cat_split) > 0:
            for category in cat_split:
                try:
                    cat_obj = IpcopAbuseCategory.objects.get(id=category)
                except IpcopAbuseCategory.DoesNotExist:
                    continue

                categories.append(cat_obj)

        bundle.obj.ip = bundle.related_obj

        if "comment" in bundle.data:
            bundle.obj.comment = bundle.data["comment"]
        else:
            bundle.obj.comment = (
                "Malicious activity detected from " + str(ip_address) + "."
            )

        # Check if IP is in filter range
        ip_filters = list(
            chain(IpcopFilterIp4.objects.all(), IpcopFilterIp6.objects.all())
        )

        for ip_filter in ip_filters:
            if ip_address in ipaddress.ip_network(ip_filter.cidr):
                return bundle

        bundle.obj.save(force_insert=True)

        bundle.obj.categories.set(categories)
        bundle.obj.save()

        return bundle
