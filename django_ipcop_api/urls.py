from django.urls import path, include
from tastypie.api import Api

from django_ipcop_api.django_ipcop_api.api.resources import *

v1_api = Api(api_name="v1")
v1_api.register(CheckIpResource())
v1_api.register(ReportResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
